package pl.datumo.sudoku.validator.app

import java.io.IOException

import com.typesafe.scalalogging.LazyLogging
import pl.datumo.sudoku.validator.domain.{BoardValidator, BoardValidationException}
import pl.datumo.sudoku.validator.infra.{BoardParser, BoardParserException}

object SudokuValidatorMain extends LazyLogging {

  val config: AppConfig = AppConfig()

  def main(args: Array[String]): Unit = {

    logger.info("Welcome in Sudoku Validator by Pawel Wojcik" )


    try {
      val filePath = args(0)
      val board = BoardParser.readFile( filePath, config.inputFileSeparator )

      val boardValidator = BoardValidator.apply( config.tableSize, config.emptyFieldNumericalValue )
      boardValidator.validate( board )


      logger.info("Provided solution is OK.")
    }
    catch {
      case e: ArrayIndexOutOfBoundsException => logger.error(s"No argument containing file path was passed to the program")
      case e: IOException => logger.error(s"File can't be read. Error message is: ${e.getMessage}" )
      case e: BoardParserException => logger.error( s"File input can't be parsed as Sudoku Board. Error message is: ${e.getMessage}" )
      case e: BoardValidationException => logger.error( s"Board is not valid. Error message is: ${e.getMessage}" )
    }

  }



}
