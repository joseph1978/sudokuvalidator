package pl.datumo.sudoku.validator

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import pl.datumo.sudoku.validator.domain.{BoardValidationException, BoardValidator}


class BoardValidatorTest extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks with Matchers {

  private final val boardSize: Int = 9
  private final val emptyFieldNumericalValue: Int = 0

  private final val correctBoard: Array[Array[Int]] = Array(
    Array(4,3,5,2,6,9,7,8,1),
    Array(6,8,2,5,7,1,4,9,3),
    Array(1,9,7,8,3,4,5,6,2),
    Array(8,2,6,1,9,5,3,4,7),
    Array(3,7,4,6,8,2,9,1,5),
    Array(9,5,1,7,4,3,6,2,8),
    Array(5,1,9,3,2,6,8,7,4),
    Array(2,4,8,9,5,7,1,3,6),
    Array(7,6,3,4,1,8,2,5,9)
  )

  private final val incorrectBoard: Array[Array[Int]] = Array(
    Array(1,3,5,2,6,9,7,8,1),
    Array(6,8,2,5,7,1,4,9,3),
    Array(1,9,7,8,3,4,5,6,2),
    Array(8,2,6,1,9,5,3,4,7),
    Array(3,7,4,6,8,2,9,1,5),
    Array(9,5,1,7,4,3,6,2,8),
    Array(5,1,9,3,2,6,8,7,4),
    Array(2,4,8,9,5,7,1,3,6),
    Array(7,6,3,4,1,8,2,5,9)
  )


  "Sudoku board size which is not a square" should "be validated as incorrect thus an exception should be thrown" in {
    val notSquareBoardSize = 5
    val thrown = intercept[BoardValidationException] {
      BoardValidator.apply(notSquareBoardSize, emptyFieldNumericalValue)
    }
    assert( thrown.getMessage === s"Board Size: $notSquareBoardSize is not a perfect square." )
  }

  "Row with values from 1 to N" should "be validated as correct" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    assert( bv.hasValues1toN(Array(1, 2, 3, 4, 5, 6, 7, 8, 9)))
  }

  "Row with empty values marked as 0" should "be validated as correct as this also proper row" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    assert( bv.hasValues1toN(Array(1, 0, 3, 0, 5, 0, 7, 8, 9)))
  }

  "Row with duplicated values within 1 to N" should "be validate as incorrect" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    assert( !bv.hasValues1toN(Array(1, 2, 4, 4, 5, 6, 7, 8, 9)))
  }

  "Row with too little values within 1 to N" should "be validated as incorrect " in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    assert( !bv.hasValues1toN(Array(1, 2, 4, 4, 7, 8, 9)))
  }

  "Row with values higher than N" should "be validated as incorrect " in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    assert( !bv.hasValues1toN(Array(1, 2, 3, 4, 7, 8, 9, 10, 11, 12)))

  }

  "Sudoku rows which are ok" should "be validated as correct thus no exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    noException should be thrownBy bv.validateRows(correctBoard)
  }

  "Sudoku rows which are not ok by duplicates" should "be validated as incorrect thus exception should be thrown " in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)
    val thrown = intercept[BoardValidationException] {
      bv.validateRows(incorrectBoard)
    }
    assert( thrown.getMessage === "Row : 0 contains incorrect data." )
  }

  "Sudoku columns which are ok" should "be validated as correct thus no exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    noException should be thrownBy bv.validateColumns(correctBoard)
  }

  "Sudoku columns which are not ok with duplicates" should "be validated as incorrect thus exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    val thrown = intercept[BoardValidationException] {
      bv.validateColumns(incorrectBoard)
    }
    assert( thrown.getMessage === "Column : 0 contains incorrect data." )
  }

  "Board with proper subtables sqrt N x sqrt N" should "be validated as correct thus no exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    noException should be thrownBy bv.validateSubtables(correctBoard)
  }

  "Board with duplicates in subtables sqrt N x sqrt N" should "be validated as incorrect thus exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    val thrown = intercept[BoardValidationException] {
      bv.validateSubtables(Array(
        Array(5, 0, 0, 0, 7, 0, 0, 0, 0),
        Array(6, 0, 0, 1, 9, 5, 0, 0, 0),
        Array(0, 9, 8, 7, 0, 0, 0, 6, 0),
        Array(8, 0, 0, 0, 6, 0, 0, 0, 3),
        Array(4, 0, 0, 8, 0, 3, 0, 0, 1),
        Array(7, 0, 0, 0, 2, 0, 0, 0, 6),
        Array(0, 6, 0, 0, 0, 0, 2, 8, 0),
        Array(0, 0, 0, 4, 1, 9, 0, 0, 5),
        Array(0, 0, 0, 0, 8, 0, 0, 7, 9)
      ))
    }
    assert( thrown.getMessage === "SubTable : 1 contains incorrect data." )
  }

  "Sudoku board which is ok" should "be validated as correct thus no exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    noException should be thrownBy bv.validate(correctBoard)
  }

  "Sudoku board which is not ok" should "be validated as incorrect thus exception should be thrown" in {
    val bv = BoardValidator.apply(boardSize, emptyFieldNumericalValue)

    val thrown = intercept[BoardValidationException] {
      bv.validate(incorrectBoard)
    }
    assert( thrown.getMessage === "Row : 0 contains incorrect data." )
  }

}
