package pl.datumo.sudoku.validator

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import pl.datumo.sudoku.validator.infra.{BoardParser, BoardParserException}


class BoardParserTest extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks {

  private final val fileSeparator = ","

  "Board parsing field to number converter" should "properly convert integer value" in {
    val out = BoardParser.toInt("1")
    assert(out == 1)
  }

  "Board parsing field to number converter" should "properly convert large integer value" in {
    val out = BoardParser.toInt("999")
    assert(out == 999)
  }

  "Board parsing field to number converter" should "convert empty field to 0" in {
    val out = BoardParser.toInt("")
    assert(out == 0)
  }


  "Board parsing field to number converter" should "fail when converting non integer value" in {
  val thrown = intercept[BoardParserException] {
    BoardParser.toInt("abc")
  }
  assert(thrown.getMessage === "Incorrect value abc is not a number in input file.")

}

  "Txt File with board" should "be parsed correctly " in {
    val board = BoardParser.readFile( getClass.getResource("/sudoku_input_correct.txt").getPath, fileSeparator )

    assert( board(1).length == 9 )

  }

  "Csv File with board" should "be parsed correctly " in {
    val board = BoardParser.readFile( getClass.getResource("/sudoku_input_correct.csv").getPath, fileSeparator )

    assert( board(1).length == 9 )

  }

  "Txt File with board with empty values and other than 1-N values" should "be properly parsed" in {
    val board = BoardParser.readFile( getClass.getResource("/sydoku_input_empty_non1_9.txt").getPath, fileSeparator)

    assert( board(0)(2) == 0 )
    assert( board(8)(7) == 521 )
  }


  "Txt File with board with non integer values" should "not be properly parsed " in {

    val thrown = intercept[BoardParserException] {
      BoardParser.readFile( getClass.getResource("/sudoku_input_non_int_values.txt").getPath, fileSeparator)
    }
    assert(thrown.getMessage === "Incorrect value - is not a number in input file.")

  }
}
