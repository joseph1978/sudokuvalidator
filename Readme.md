Sudoku Validator - the purpose of the project is provide command line  tool for verification 
of validity of sudoku board solution.

Sudoku Validator tool is a command line tool, to run it please follow steps:
- go to project main folder (top one)
- rename runSudokuValidator.sh_backup/runSudokuValidator.bat_backup to runSudokuValidator.sh/runSudokuValidator.bat
- run runSudokuValidator.sh/bat with absolute path to input file as a param, exp.:
```
runSudokuValidator.sh src/test/resources/sudoku_input_correct.csv
```

Sudoku Validator project assumptions:
- Input file can contain empty values, which will be converted to 0 values
- Table size shout be square of integer value, input array should be a square array
- Non integer input values are recognized as not correct ones

Technical assumptions:
- DDD package model (domain, app, infra)
- Tests are based on test resources and unit testing with private package modifier (in order not to have too open methods)
- Printing output is done by logger and logback backend (lazy logging)
- Exceptions are all handled in main function in order to present output of the program and not fail - this is done to match exercise demands
- Reference conf is used to keep configuration out of the codebase and not hardcoded
- Solution can support bigger sudoku boards with perfect square size of rows and cols